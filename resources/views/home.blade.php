@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center btn-box">
                    <a class="white-link" href="/lead">
                        <button class="btn btn-primary request-btn">
                            Send Request
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
