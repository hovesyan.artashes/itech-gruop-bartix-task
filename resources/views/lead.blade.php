@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
            <div class="mt-4 text-center">
                <h2 class="text-center mb-4">Заполните форму</h2>
                <form method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" id="inputName" aria-describedby="emailHelp" placeholder="Имя *">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="inputPassword1" placeholder="Фамилия">
                    </div>
                    <div class="form-group">
                        <input required type="password" class="form-control" id="inputEmail" placeholder="E-mail *">
                    </div>
                    <div class="form-group">
                        <textarea type="password" class="form-control" id="inputMassage" placeholder="Текст сообщения"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary ">Submit</button>
                </form>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
</div>
@endsection
